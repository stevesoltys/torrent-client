package com.stevesoltys.torrentclient.model;

/**
 * Represents a tracker.
 *
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class Tracker {

    /**
     * The annnounce url.
     */
    private final String announceUrl;

    /**
     * The current tracker data for this tracker.
     */
    private TrackerData data;

    public Tracker(String announceUrl) {
        this.announceUrl = announceUrl;
        this.data = null;
    }

    public String getAnnounceUrl() {
        return announceUrl;
    }

    public TrackerData getData() {
        return data;
    }

    public void setData(TrackerData data) {
        this.data = data;
    }
}
