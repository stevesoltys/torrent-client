package com.stevesoltys.torrentclient.model;

/**
 * Represents a peer.
 *
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class Peer {

    private final String id;
    private final String address;
    private final int port;

    public Peer(String id, String address, int port) {
        this.id = id;
        this.address = address;
        this.port = port;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Peer peer = (Peer) o;

        if (port != peer.port) return false;
        if (id != null ? !id.equals(peer.id) : peer.id != null) return false;
        return !(address != null ? !address.equals(peer.address) : peer.address != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + port;
        return result;
    }

    public String getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public int getPort() {
        return port;
    }
}
