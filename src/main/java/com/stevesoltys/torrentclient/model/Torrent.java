package com.stevesoltys.torrentclient.model;

import com.stevesoltys.torrentclient.io.TorrentChunk;
import com.stevesoltys.torrentclient.util.StringUtil;
import com.stevesoltys.torrentclient.util.TorrentInfo;

import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;

/**
 * Represents a torrent.
 *
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class Torrent {

    /**
     * The tracker.
     */
    private final Tracker tracker;

    /**
     * The info hash data for this torrent.
     */
    private final ByteBuffer infoHash;

    /**
     * The length of the file for this torrent.
     */
    private final int fileLength;

    /**
     * The size of a chunk for this torrent.
     */
    private final int chunkSize;

    /**
     * The list of chunks for this torrent.
     */
    private final List<TorrentChunk> chunks;

    @SuppressWarnings("unchecked")
    public Torrent(TorrentInfo torrentInfo) {
        this.tracker = new Tracker(torrentInfo.announce_url.toString());
        this.infoHash = torrentInfo.info_hash;

        this.fileLength = torrentInfo.file_length;

        this.chunkSize = torrentInfo.piece_length;
        this.chunks = createChunks(torrentInfo);
    }

    /**
     * Generates a list of chunks for this torrent, given the decoded torrent file data.
     *
     * @param torrentInfo The decoded torrent info.
     * @return The list of chunks.
     */
    private List<TorrentChunk> createChunks(TorrentInfo torrentInfo) {
        List<TorrentChunk> chunks = new LinkedList<>();

        int length = torrentInfo.file_length;
        int offset = 0;
        int index = 0;

        while (offset < length) {
            int chunkLength = (offset + chunkSize < length) ? chunkSize : length - offset;

            chunks.add(new TorrentChunk(torrentInfo.piece_hashes[index], index, chunkLength));

            offset += chunkLength;
            index++;
        }

        return chunks;
    }

    public ByteBuffer getInfoHash() {
        return infoHash.duplicate();
    }

    public String getEscapedInfoHash() {
        return StringUtil.encodeHex(infoHash.array());
    }

    public Tracker getTracker() {
        return tracker;
    }

    public int getChunkSize() {
        return chunkSize;
    }

    public List<TorrentChunk> getChunks() {
        return chunks;
    }

    public int getFileLength() {
        return fileLength;
    }
}
