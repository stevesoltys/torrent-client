package com.stevesoltys.torrentclient.model;

import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Contains miscellaneous data obtained from a {@link Tracker}.
 *
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class TrackerData {

    /**
     * A list of {@link Peer}s that are currently in our swarm.
     */
    private final List<Peer> peers;

    /**
     * The interval between performing tracker queries.
     */
    private final int interval;

    public TrackerData(Map<String, Object> dictionary) {
        this.peers = parsePeerList(dictionary);
        this.interval = (int) dictionary.get("interval");
    }

    /**
     * Parses the peer list, given the Bencode dictionary.
     *
     * @param dictionary The decoded dictionary.
     * @return The list of peers.
     */
    @SuppressWarnings("unchecked")
    private List<Peer> parsePeerList(Map<String, Object> dictionary) {
        List<Peer> peers = new LinkedList<>();

        List<Map<ByteBuffer, Object>> peerDictionary = (List<Map<ByteBuffer, Object>>) dictionary.get("peers");

        for (Map<ByteBuffer, Object> peer : peerDictionary) {
            String id = new String(((ByteBuffer) peer.get(ByteBuffer.wrap("peer id".getBytes()))).array());
            String address = new String(((ByteBuffer) peer.get(ByteBuffer.wrap("ip".getBytes()))).array());
            int port = (int) peer.get(ByteBuffer.wrap("port".getBytes()));

            peers.add(new Peer(id, address, port));
        }


        return peers;
    }

    public List<Peer> getPeers() {
        return peers;
    }

    public long getInterval() {
        return interval;
    }
}
