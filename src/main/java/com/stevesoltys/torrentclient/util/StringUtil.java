package com.stevesoltys.torrentclient.util;

/**
 * Utilities used for string manipulation.
 *
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class StringUtil {

    /**
     * An array of hex characters.
     */
    private static final char[] HEX_CHARS = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    /**
     * Encodes the given bytes into their url-friendly hex string.
     *
     * @param bytes The byte array.
     * @return The encoded string.
     */
    public static String encodeHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder(bytes.length * 3);

        for (byte b : bytes) {
            byte hi = (byte) ((b >> 4) & 0x0f);
            byte lo = (byte) (b & 0x0f);

            sb.append('%').append(HEX_CHARS[hi]).append(HEX_CHARS[lo]);
        }
        return sb.toString();
    }
}
