package com.stevesoltys.torrentclient.util;

import java.util.concurrent.*;

/**
 * A utility class which allows for the scheduling of tasks in an {@link ExecutorService}.
 *
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class ThreadPool {

    /**
     * The singleton object for this class.
     */
    private static final ThreadPool singleton = new ThreadPool();

    public static ThreadPool getSingleton() {
        return singleton;
    }

    /**
     * The executor service.
     */
    private final ThreadPoolExecutor service = new ThreadPoolExecutor(5, 5, 60,
            TimeUnit.SECONDS, new LinkedBlockingQueue<>());

    public ThreadPool() {
        service.allowCoreThreadTimeOut(true);
    }

    //Executors.newCachedThreadPool();

    /**
     * Submits the given task to the {@link ExecutorService}.
     *
     * @param task The task.
     * @return The future for the given task.
     */
    public Future<?> submit(Runnable task) {
        return service.submit(task);
    }

    /**
     * Stops the executor service.
     */
    public void shutdown() {
        service.shutdown();
    }
}
