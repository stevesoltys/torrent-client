package com.stevesoltys.torrentclient.net.peer.message.codec;

import com.stevesoltys.torrentclient.net.peer.message.Message;

import java.nio.ByteBuffer;

/**
 * A decoder for a {@link Message}.
 *
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public interface MessageDecoder<T extends Message> {

    T decode(ByteBuffer packet);
}
