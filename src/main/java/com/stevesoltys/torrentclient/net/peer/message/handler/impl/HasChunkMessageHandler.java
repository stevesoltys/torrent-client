package com.stevesoltys.torrentclient.net.peer.message.handler.impl;

import com.stevesoltys.torrentclient.net.peer.PeerSession;
import com.stevesoltys.torrentclient.net.peer.message.handler.MessageHandler;
import com.stevesoltys.torrentclient.net.peer.message.impl.HasChunkMessage;

/**
 * @author Steve Soltys
 */
public class HasChunkMessageHandler extends MessageHandler<HasChunkMessage> {
    @Override
    public void handle(PeerSession session, HasChunkMessage message) {
        if (message.getIndex() < session.getBitfield().length) {
            session.getBitfield()[message.getIndex()] = true;
        }
    }
}
