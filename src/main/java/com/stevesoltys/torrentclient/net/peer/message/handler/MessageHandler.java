package com.stevesoltys.torrentclient.net.peer.message.handler;

import com.stevesoltys.torrentclient.net.peer.PeerSession;
import com.stevesoltys.torrentclient.net.peer.message.Message;

/**
 * A handler for a {@link Message}.
 *
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public abstract class MessageHandler<T extends Message> {

    /**
     * Handles a {@link Message}, given the {@link PeerSession}.
     *
     * @param session The session.
     * @param message The message.
     */
    public abstract void handle(PeerSession session, T message);
}
