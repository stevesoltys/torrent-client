package com.stevesoltys.torrentclient.net.peer.message.impl;

import com.stevesoltys.torrentclient.net.peer.message.Message;

/**
 *
 *
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class BitfieldMessage extends Message {

    private final byte[] bitfield;

    public BitfieldMessage(byte[] bitfield) {
        this.bitfield = bitfield;
    }

    public byte[] getBitfield() {
        return bitfield;
    }
}
