package com.stevesoltys.torrentclient.net.peer.message.codec.impl;

import com.stevesoltys.torrentclient.net.peer.message.codec.MessageDecoder;
import com.stevesoltys.torrentclient.net.peer.message.impl.ChunkMessage;

import java.nio.ByteBuffer;

/**
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class ChunkMessageDecoder implements MessageDecoder<ChunkMessage> {
    @Override
    public ChunkMessage decode(ByteBuffer packet) {

        int index = packet.getInt();
        int begin = packet.getInt();

        byte[] block = new byte[packet.remaining()];
        packet.get(block);

        return new ChunkMessage(index, begin, block);
    }
}
