package com.stevesoltys.torrentclient.net.peer.message.codec.impl;

import com.stevesoltys.torrentclient.net.peer.message.codec.MessageEncoder;
import com.stevesoltys.torrentclient.net.peer.message.impl.BitfieldMessage;

import java.nio.ByteBuffer;

/**
 * @author Steve Soltys
 */
public class BitfieldMessageEncoder implements MessageEncoder<BitfieldMessage> {

    @Override
    public ByteBuffer encode(BitfieldMessage message) {

        ByteBuffer buffer = ByteBuffer.allocate(1 + message.getBitfield().length);
        buffer.put((byte) 5);
        buffer.put(message.getBitfield());

        return buffer;
    }
}
