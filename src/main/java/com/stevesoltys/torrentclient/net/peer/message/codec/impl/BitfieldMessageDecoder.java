package com.stevesoltys.torrentclient.net.peer.message.codec.impl;

import com.stevesoltys.torrentclient.net.peer.message.codec.MessageDecoder;
import com.stevesoltys.torrentclient.net.peer.message.impl.BitfieldMessage;

import java.nio.ByteBuffer;

/**
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class BitfieldMessageDecoder implements MessageDecoder<BitfieldMessage> {
    @Override
    public BitfieldMessage decode(ByteBuffer packet) {

        byte[] bitfield = new byte[packet.remaining()];
        packet.get(bitfield);

        return new BitfieldMessage(bitfield);
    }
}
