package com.stevesoltys.torrentclient.net.peer.message.codec.impl;

import com.stevesoltys.torrentclient.net.peer.message.codec.MessageEncoder;
import com.stevesoltys.torrentclient.net.peer.message.impl.ChunkMessage;

import java.nio.ByteBuffer;

/**
 * @author Steve Soltys
 */
public class ChunkMessageEncoder implements MessageEncoder<ChunkMessage> {
    @Override
    public ByteBuffer encode(ChunkMessage message) {

        ByteBuffer buffer = ByteBuffer.allocate(9 + message.getData().length);

        buffer.put((byte) 7);
        buffer.putInt(message.getIndex());
        buffer.putInt(message.getBegin());
        buffer.put(message.getData());

        return buffer;
    }
}
