package com.stevesoltys.torrentclient.net.peer.message;

import com.stevesoltys.torrentclient.net.peer.PeerSession;

/**
 * A message that a {@link PeerSession} can receive or send.
 *
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public abstract class Message {
}
