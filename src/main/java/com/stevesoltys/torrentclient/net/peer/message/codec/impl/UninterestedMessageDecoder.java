package com.stevesoltys.torrentclient.net.peer.message.codec.impl;

import com.stevesoltys.torrentclient.net.peer.message.codec.MessageDecoder;
import com.stevesoltys.torrentclient.net.peer.message.impl.UninterestedMessage;

import java.nio.ByteBuffer;

/**
 * @author Steve Soltys
 */
public class UninterestedMessageDecoder implements MessageDecoder<UninterestedMessage> {
    @Override
    public UninterestedMessage decode(ByteBuffer packet) {
        return new UninterestedMessage();
    }
}
