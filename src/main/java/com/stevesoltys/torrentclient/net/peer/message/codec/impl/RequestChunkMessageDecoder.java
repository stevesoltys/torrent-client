package com.stevesoltys.torrentclient.net.peer.message.codec.impl;

import com.stevesoltys.torrentclient.net.peer.message.codec.MessageDecoder;
import com.stevesoltys.torrentclient.net.peer.message.impl.RequestChunkMessage;

import java.nio.ByteBuffer;

/**
 * @author Steve Soltys
 */
public class RequestChunkMessageDecoder implements MessageDecoder<RequestChunkMessage> {
    @Override
    public RequestChunkMessage decode(ByteBuffer packet) {

        int index = packet.getInt();
        int begin = packet.getInt();
        int length = packet.getInt();

        return new RequestChunkMessage(index, begin, length);
    }
}
