package com.stevesoltys.torrentclient.net.peer.message.handler;

import com.stevesoltys.torrentclient.net.peer.message.Message;
import com.stevesoltys.torrentclient.net.peer.message.handler.impl.*;
import com.stevesoltys.torrentclient.net.peer.message.impl.*;

import java.util.HashMap;
import java.util.Map;

/**
 * A registry for {@link MessageHandler}s.
 *
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class MessageHandlerRegistry {

    /**
     * The singleton object for this class.
     */
    private static final MessageHandlerRegistry singleton = new MessageHandlerRegistry();

    public static MessageHandlerRegistry getSingleton() {
        return singleton;
    }

    /**
     * A map of {@link Message} classes and their respective handlers.
     */
    private final Map<Class, MessageHandler> handlers = new HashMap<>();

    private MessageHandlerRegistry() {
        register();
    }

    /**
     * Registers the {@link MessageHandler}s.
     */
    private void register() {
        handlers.put(BitfieldMessage.class, new BitfieldMessageHandler());
        handlers.put(ChokeMessage.class, new ChokeMessageHandler());
        handlers.put(UnchokeMessage.class, new UnchokeMessageHandler());
        handlers.put(ChunkMessage.class, new ChunkMessageHandler());
        handlers.put(RequestChunkMessage.class, new RequestChunkMessageHandler());
        handlers.put(InterestedMessage.class, new InterestedMessageHandler());
        handlers.put(UninterestedMessage.class, new UninterestedMessageHandler());
        handlers.put(HasChunkMessage.class, new HasChunkMessageHandler());
    }

    public MessageHandler getHandler(Class clazz) {
        return handlers.get(clazz);
    }
}
