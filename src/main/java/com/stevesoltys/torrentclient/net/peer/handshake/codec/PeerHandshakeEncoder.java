package com.stevesoltys.torrentclient.net.peer.handshake.codec;

import com.stevesoltys.torrentclient.net.peer.handshake.PeerHandshake;
import com.stevesoltys.torrentclient.net.peer.handshake.PeerHandshakeConstants;
import com.stevesoltys.torrentclient.net.tracker.TorrentSession;

import java.nio.ByteBuffer;

/**
 * An encoder for the {@link PeerHandshake}.
 *
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class PeerHandshakeEncoder {

    /**
     * The torrent session.
     */
    private final TorrentSession torrentSession;

    public PeerHandshakeEncoder(TorrentSession torrentSession) {
        this.torrentSession = torrentSession;
    }

    /**
     * Encodes a handshake using the data from our {@link TorrentSession}.
     *
     * @return A buffer containing the encoded handshake.
     */
    public ByteBuffer encode() {
        ByteBuffer buffer = ByteBuffer.allocate(PeerHandshakeConstants.HANDSHAKE_LENGTH);

        buffer.put(PeerHandshakeConstants.HEADER_BYTE);
        buffer.put(PeerHandshakeConstants.HEADER_TEXT.getBytes());
        buffer.put(new byte[PeerHandshakeConstants.RESERVED_BYTES_LENGTH]);
        buffer.put(torrentSession.getTorrent().getInfoHash());
        buffer.put(torrentSession.getPeerId().getBytes());

        return buffer;
    }
}
