package com.stevesoltys.torrentclient.net.peer.message.codec.impl;

import com.stevesoltys.torrentclient.net.peer.message.codec.MessageEncoder;
import com.stevesoltys.torrentclient.net.peer.message.impl.InterestedMessage;

import java.nio.ByteBuffer;

/**
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class InterestedMessageEncoder implements MessageEncoder<InterestedMessage> {

    @Override
    public ByteBuffer encode(InterestedMessage message) {

        ByteBuffer buffer = ByteBuffer.allocate(1);
        buffer.put((byte) 2);

        return buffer;
    }
}
