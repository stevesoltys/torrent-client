package com.stevesoltys.torrentclient.net.peer.message.handler.impl;

import com.stevesoltys.torrentclient.net.peer.PeerSession;
import com.stevesoltys.torrentclient.net.peer.message.handler.MessageHandler;
import com.stevesoltys.torrentclient.net.peer.message.impl.BitfieldMessage;
import com.stevesoltys.torrentclient.net.peer.message.impl.InterestedMessage;

/**
 * @author Steve Soltys
 */
public class BitfieldMessageHandler extends MessageHandler<BitfieldMessage> {

    @Override
    public void handle(PeerSession session, BitfieldMessage message) {

//        System.out.println("Got bitfield message for " + session.getPeer().getId());

//        byte[] bitfield = message.getBitfield();
//
//        for (int i = 0; i < bitfield.length; i++) {
//            int index = i / 8;
//            int position = index % 8;
//
//            session.getBitfield()[i] = (-bitfield[index] >> position & 1) == 1;
//        }

        session.send(new InterestedMessage());
    }
}
