package com.stevesoltys.torrentclient.net.peer.message.codec.impl;

import com.stevesoltys.torrentclient.net.peer.message.codec.MessageDecoder;
import com.stevesoltys.torrentclient.net.peer.message.impl.ChokeMessage;

import java.nio.ByteBuffer;

/**
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class ChokeMessageDecoder implements MessageDecoder<ChokeMessage> {

    @Override
    public ChokeMessage decode(ByteBuffer packet) {
        return new ChokeMessage();
    }
}
