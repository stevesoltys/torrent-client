package com.stevesoltys.torrentclient.net.peer.message.codec.impl;

import com.stevesoltys.torrentclient.net.peer.message.codec.MessageEncoder;
import com.stevesoltys.torrentclient.net.peer.message.impl.HasChunkMessage;

import java.nio.ByteBuffer;

/**
 * @author Steve Soltys
 */
public class HasChunkMessageEncoder implements MessageEncoder<HasChunkMessage> {
    @Override
    public ByteBuffer encode(HasChunkMessage message) {

        ByteBuffer buffer = ByteBuffer.allocate(5);
        buffer.put((byte) 4);
        buffer.putInt(message.getIndex());

        return buffer;
    }
}
