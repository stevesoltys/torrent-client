package com.stevesoltys.torrentclient.net.peer.message.handler.impl;

import com.stevesoltys.torrentclient.io.TorrentChunk;
import com.stevesoltys.torrentclient.io.TorrentProgress;
import com.stevesoltys.torrentclient.model.Torrent;
import com.stevesoltys.torrentclient.net.peer.PeerSession;
import com.stevesoltys.torrentclient.net.peer.message.handler.MessageHandler;
import com.stevesoltys.torrentclient.net.peer.message.impl.ChunkMessage;
import com.stevesoltys.torrentclient.net.peer.message.impl.RequestChunkMessage;

import java.util.Arrays;

/**
 * @author Steve Soltys
 */
public class RequestChunkMessageHandler extends MessageHandler<RequestChunkMessage> {
    @Override
    public void handle(PeerSession session, RequestChunkMessage message) {

        System.out.println("Got request for chunk " + message.getIndex() + " from: " + session.getPeer().getId());

        int index = message.getIndex();
        int begin = message.getBegin();
        int length = message.getLength();

        Torrent torrent = session.getTorrentSesssion().getTorrent();
        TorrentChunk chunk = torrent.getChunks().get(index);

        if (chunk.isVerified()) {
            // Make sure the data is within the bounds of the chunk
            if (begin >= 0 && begin + length <= chunk.getLength()) {
                byte[] data = Arrays.copyOfRange(chunk.getData().array(), begin, begin + length);

                if (!session.isPeerChoked()) {
                    TorrentProgress torrentProgress = session.getTorrentSesssion().getFileManager().
                            getTorrentProgress();

                    torrentProgress.increaseUploaded(length);
                    session.send(new ChunkMessage(index, begin, data));
                }
            }
        }
    }
}
