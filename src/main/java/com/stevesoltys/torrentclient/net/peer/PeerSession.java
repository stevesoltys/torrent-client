package com.stevesoltys.torrentclient.net.peer;

import com.stevesoltys.torrentclient.io.TorrentChunk;
import com.stevesoltys.torrentclient.model.Peer;
import com.stevesoltys.torrentclient.net.peer.message.Message;
import com.stevesoltys.torrentclient.net.peer.message.codec.MessageCodecRegistry;
import com.stevesoltys.torrentclient.net.peer.message.codec.MessageDecoder;
import com.stevesoltys.torrentclient.net.peer.message.codec.MessageEncoder;
import com.stevesoltys.torrentclient.net.peer.message.handler.MessageHandler;
import com.stevesoltys.torrentclient.net.peer.message.handler.MessageHandlerRegistry;
import com.stevesoltys.torrentclient.net.peer.message.impl.*;
import com.stevesoltys.torrentclient.net.tracker.TorrentSession;
import com.stevesoltys.torrentclient.net.tracker.TorrentSessionState;
import com.stevesoltys.torrentclient.util.ThreadPool;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * A session initiated between the local host and a {@link Peer}.
 *
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class PeerSession {

    /**
     * The torrent session.
     */
    private final TorrentSession torrentSession;

    /**
     * The peer that we are connected to.
     */
    private final Peer peer;

    /**
     * The socket.
     */
    private final Socket socket;

    /**
     * A flag indicating whether we are choked from downloading.
     */
    private boolean choked;

    /**
     * A flag indicating whether the peer is choked from downloading.
     */
    private boolean peerChoked;

    /**
     * An {@link Optional} field which may contain the current chunk we have requested from the {@link Peer}.
     */
    private Optional<TorrentChunk> currentChunk;

    /**
     * The bitfield for this peer.
     */
    private boolean[] bitfield;

    /**
     * The message queue for this peer.
     */
    private final Queue<Message> messageQueue;

    public PeerSession(TorrentSession torrentSession, Peer peer, Socket socket) {
        this.torrentSession = torrentSession;
        this.peer = peer;
        this.socket = socket;
        this.currentChunk = Optional.empty();
        this.messageQueue = new LinkedBlockingQueue<>();
        this.bitfield = new boolean[torrentSession.getTorrent().getChunks().size()];
        this.choked = true;
        this.peerChoked = true;
    }

    /**
     * Starts listening and sending messages between our machine and the {@link Peer} on another thread.
     */
    @SuppressWarnings("unchecked")
    public void start() {

        ThreadPool.getSingleton().submit(() -> {

            boolean[] ourBitfield = torrentSession.getFileManager().getBitfield();

            for (int i = 0; i < ourBitfield.length; i++) {
                if (ourBitfield[i]) {
                    send(new HasChunkMessage(i));
                }
            }

//            send(new InterestedMessage());

            while (torrentSession.getState() != TorrentSessionState.STOPPED && !socket.isClosed()) {

                try {
                    DataInputStream inputStream = new DataInputStream(socket.getInputStream());

                    int messageLength = inputStream.readInt();

                    if (messageLength > 0) {
                        ByteBuffer rawMessage = ByteBuffer.allocate(messageLength);

                        while (rawMessage.position() < messageLength) {
                            rawMessage.put((byte) inputStream.read());
                        }

                        rawMessage.position(0);

                        MessageCodecRegistry registry = MessageCodecRegistry.getSingleton();

                        int messageId = rawMessage.get();

                        MessageDecoder decoder = registry.getDecoder(messageId);

                        if (decoder != null) {
                            Message message = decoder.decode(rawMessage);

                            MessageHandlerRegistry handlerRegistry = MessageHandlerRegistry.getSingleton();
                            MessageHandler handler = handlerRegistry.getHandler(message.getClass());

                            handler.handle(this, message);
                        } else {
                            System.out.println("No decoder found for message '" + messageId + "'.");
                        }
                    } else {
                        send(new KeepAliveMessage());
                    }

                } catch (IOException e) {
                    stop();
                }

            }

        });

        ThreadPool.getSingleton().submit(() -> {

            currentChunk = getNextChunk();

            while (torrentSession.getState() != TorrentSessionState.STOPPED && !socket.isClosed()) {

                try {
                    if (!choked && torrentSession.getState() == TorrentSessionState.DOWNLOADING) {

                        if (currentChunk.isPresent()) {
                            TorrentChunk chunk = currentChunk.get();

                            int index = chunk.getIndex();
                            int begin = chunk.getData().position();
                            int length = chunk.getData().limit() - begin;

                            if (length > 0) {
                                if (length > RequestChunkMessage.MAX_REQUEST_LENGTH) {
                                    length = RequestChunkMessage.MAX_REQUEST_LENGTH;
                                }

                                send(new RequestChunkMessage(index, begin, length));
                                currentChunk = Optional.empty();
                            } else {

                                boolean success = chunk.verify();

                                if (success) {
                                    for (PeerSession session : torrentSession.getSessions().values()) {
                                        session.send(new HasChunkMessage(chunk.getIndex()));
                                    }

                                    chunk.setVerified(true);
                                    currentChunk = getNextChunk();
                                } else {
                                    System.err.println("Error verifying chunk: " + chunk.getIndex());
                                    chunk.getData().clear();
                                }

                            }
                        }

                        if (torrentSession.getFileManager().completed()) {
                            torrentSession.setState(TorrentSessionState.COMPLETED);
                        }
                    }

                    while (!messageQueue.isEmpty()) {
                        write(messageQueue.poll());
                    }

                    Thread.sleep(1);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                    stop();
                }

            }

        });

        ThreadPool.getSingleton().submit(() -> {
            while (torrentSession.getState() != TorrentSessionState.STOPPED) {
                try {
                    if (!choked && torrentSession.getState() == TorrentSessionState.DOWNLOADING) {
                        if (!currentChunk.isPresent()) {
                            currentChunk = getNextChunk();
                        }
                    }

                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Stops this session by closing the {@link Socket}.
     */
    public void stop() {
        try {
            if (socket != null) {
                socket.close();
            }

            torrentSession.unregisterPeer(peer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sends the given {@link Message} to the {@link Peer}.
     *
     * @param message The message.
     * @throws IOException If there was an error while writing the message.
     */
    public void send(Message message) {
        messageQueue.add(message);
    }

    /**
     * Gets the next torrent chunk for this peer.
     *
     * @return The next torrent chunk.
     */
    private Optional<TorrentChunk> getNextChunk() {
        List<TorrentChunk> chunks = torrentSession.getTorrent().getChunks();

        for (int i = 0; i < bitfield.length; i++) {
            if (bitfield[i] && !chunks.get(i).isVerified()) {
                return Optional.of(chunks.get(i));
            }
        }

        return torrentSession.getFileManager().getRandomIncompleteChunk();
    }

    /**
     * Writes the given {@link Message} to the output stream.
     *
     * @param message The message.
     */
    private void write(Message message) {
        try {
            DataOutputStream outputStream = new DataOutputStream(socket.getOutputStream());

            MessageCodecRegistry registry = MessageCodecRegistry.getSingleton();
            MessageEncoder encoder = registry.getEncoder(message.getClass());

            if (encoder != null) {
                byte[] data = encoder.encode(message).array();
                outputStream.writeInt(data.length);
                outputStream.write(data);
            } else {
                System.err.println("Could not find encoder for message: " + message);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            stop();
        }
    }

    public Socket getSocket() {
        return socket;
    }

    public Peer getPeer() {
        return peer;
    }

    public TorrentSession getTorrentSesssion() {
        return torrentSession;
    }

    public boolean isChoked() {
        return choked;
    }

    public void setChoked(boolean choked) {
        this.choked = choked;
    }

    public boolean isPeerChoked() {
        return peerChoked;
    }

    public void setPeerChoked(boolean peerChoked) {
        this.peerChoked = peerChoked;
    }

    public Optional<TorrentChunk> getCurrentChunk() {
        return currentChunk;
    }

    public void setCurrentChunk(Optional<TorrentChunk> currentChunk) {
        this.currentChunk = currentChunk;
    }

    public boolean[] getBitfield() {
        return bitfield;
    }
}
