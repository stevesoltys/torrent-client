package com.stevesoltys.torrentclient.net.peer.message.codec;

import com.stevesoltys.torrentclient.net.peer.message.Message;
import com.stevesoltys.torrentclient.net.peer.message.codec.impl.*;
import com.stevesoltys.torrentclient.net.peer.message.impl.*;

import java.util.HashMap;
import java.util.Map;

/**
 * A registry for {@link MessageDecoder}s and {@link MessageEncoder}s.
 *
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class MessageCodecRegistry {

    /**
     * The singleton object for this class.
     */
    private static final MessageCodecRegistry singleton = new MessageCodecRegistry();

    public static MessageCodecRegistry getSingleton() {
        return singleton;
    }

    private final Map<Integer, Class> messageIds = new HashMap<>();

    /**
     * A map of {@link Message} ids and their respective decoders.
     */
    private final Map<Integer, MessageDecoder> decoders = new HashMap<>();

    /**
     * A map of {@link Message} classes and their respective encoders.
     */
    private final Map<Class, MessageEncoder> encoders = new HashMap<>();

    private MessageCodecRegistry() {
        registerDecoders();
        registerEncoders();
    }

    /**
     * Registers the {@link MessageDecoder}s.
     */
    private void registerDecoders() {
        decoders.put(0, new ChunkMessageDecoder());
        decoders.put(1, new UnchokeMessageDecoder());
        decoders.put(2, new InterestedMessageDecoder());
        decoders.put(3, new UninterestedMessageDecoder());
        decoders.put(4, new HasChunkMessageDecoder());
        decoders.put(5, new BitfieldMessageDecoder());
        decoders.put(6, new RequestChunkMessageDecoder());
        decoders.put(7, new ChunkMessageDecoder());
    }

    /**
     * Registers the {@link MessageEncoder}s.
     */
    private void registerEncoders() {
        encoders.put(InterestedMessage.class, new InterestedMessageEncoder());
        encoders.put(UninterestedMessage.class, new UninterestedMessageEncoder());
        encoders.put(KeepAliveMessage.class, new KeepAliveMessageEncoder());
        encoders.put(RequestChunkMessage.class, new RequestChunkMessageEncoder());
        encoders.put(BitfieldMessage.class, new BitfieldMessageEncoder());
        encoders.put(HasChunkMessage.class, new HasChunkMessageEncoder());
        encoders.put(ChunkMessage.class, new ChunkMessageEncoder());
        encoders.put(UnchokeMessage.class, new UnchokeMessageEncoder());
    }

    public MessageEncoder getEncoder(Class clazz) {
        return encoders.get(clazz);
    }

    public MessageDecoder getDecoder(int messageId) {
        return decoders.get(messageId);
    }
}
