package com.stevesoltys.torrentclient.net.peer.handshake;

/**
 * Constants used while performing a {@link PeerHandshake}.
 *
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class PeerHandshakeConstants {

    /**
     * The length of a handshake, in bytes.
     */
    public static final int HANDSHAKE_LENGTH = 68;

    /**
     * The first byte sent in the BitTorrent handshake.
     */
    public static final byte HEADER_BYTE = 19;

    /**
     * The text sent after the first byte in the BitTorrent handshake.
     */
    public static final String HEADER_TEXT = "BitTorrent protocol";

    /**
     * The length of the header text.
     */
    public static final int HEADER_TEXT_LENGTH = HEADER_TEXT.getBytes().length;

    /**
     * The number of bytes reserved in the BitTorrent handshake.
     */
    public static final int RESERVED_BYTES_LENGTH = 8;

    /**
     * The length of an SHA-1 'info' hash.
     */
    public static final int INFO_HASH_LENGTH = 20;

    /**
     * The length of a peer id.
     */
    public static final int PEER_ID_LENGTH = 20;
}
