package com.stevesoltys.torrentclient.net.peer.message.codec.impl;

import com.stevesoltys.torrentclient.net.peer.message.codec.MessageEncoder;
import com.stevesoltys.torrentclient.net.peer.message.impl.UnchokeMessage;

import java.nio.ByteBuffer;

/**
 * @author Steve Soltys
 */
public class UnchokeMessageEncoder implements MessageEncoder<UnchokeMessage> {
    @Override
    public ByteBuffer encode(UnchokeMessage message) {

        ByteBuffer buffer = ByteBuffer.allocate(1);
        buffer.put((byte) 1);

        return buffer;
    }
}
