package com.stevesoltys.torrentclient.net.peer.message.impl;

import com.stevesoltys.torrentclient.io.TorrentChunk;
import com.stevesoltys.torrentclient.model.Peer;
import com.stevesoltys.torrentclient.net.peer.message.Message;

/**
 * A message which tells the receiving {@link Peer} that a {@link TorrentChunk} is available for request.
 *
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class HasChunkMessage extends Message {

    /**
     * The {@link TorrentChunk} index.
     */
    private final int index;

    public HasChunkMessage(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }
}
