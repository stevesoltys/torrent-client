package com.stevesoltys.torrentclient.net.peer.message.codec.impl;

import com.stevesoltys.torrentclient.net.peer.message.codec.MessageEncoder;
import com.stevesoltys.torrentclient.net.peer.message.impl.UninterestedMessage;

import java.nio.ByteBuffer;

/**
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class UninterestedMessageEncoder implements MessageEncoder<UninterestedMessage> {

    @Override
    public ByteBuffer encode(UninterestedMessage message) {

        ByteBuffer buffer = ByteBuffer.allocate(1);
        buffer.put((byte) 3);

        return buffer;
    }
}
