package com.stevesoltys.torrentclient.net.peer.handshake.codec;

import com.stevesoltys.torrentclient.net.peer.PeerSession;
import com.stevesoltys.torrentclient.net.peer.handshake.PeerHandshakeConstants;
import com.stevesoltys.torrentclient.net.peer.handshake.PeerHandshake;
import com.stevesoltys.torrentclient.net.tracker.TorrentSession;

import java.nio.ByteBuffer;

/**
 * A decoder for the {@link PeerHandshake}.
 *
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class PeerHandshakeDecoder {

    /**
     * The torrent session.
     */
    private final TorrentSession torrentSession;

    /**
     * The peer session.
     */
    private final PeerSession peerSession;

    public PeerHandshakeDecoder(TorrentSession torrentSession, PeerSession peerSession) {
        this.torrentSession = torrentSession;
        this.peerSession = peerSession;
    }

    /**
     * Decodes the given handshake buffer.
     *
     * @param buffer A buffer containing the incoming handshake data.
     * @return A flag indicating whether or not the handshake was successful.
     */
    public boolean decode(ByteBuffer buffer) {

        byte headerByte = buffer.get();

        if (headerByte != PeerHandshakeConstants.HEADER_BYTE) {
            System.err.println("Invalid header byte during peer handshake: " + headerByte);
            return false;
        }

        byte[] header = new byte[PeerHandshakeConstants.HEADER_TEXT_LENGTH];
        buffer.get(header);

        String headerString = new String(header);

        if (!headerString.equals(PeerHandshakeConstants.HEADER_TEXT)) {
            System.err.println("Invalid header during peer handshake: " + headerString);
            return false;
        }

        buffer.get(new byte[PeerHandshakeConstants.RESERVED_BYTES_LENGTH]);

        byte[] infoHashBuffer = new byte[PeerHandshakeConstants.INFO_HASH_LENGTH];
        buffer.get(infoHashBuffer);
        ByteBuffer infoHash = ByteBuffer.wrap(infoHashBuffer);

        ByteBuffer actualInfoHash = torrentSession.getTorrent().getInfoHash();

        for (int i = 0; i < PeerHandshakeConstants.INFO_HASH_LENGTH; i++) {
            if (actualInfoHash.get(i) != infoHash.get(i)) {
                System.err.println("Invalid info hash during peer handshake.");
                return false;
            }
        }

        byte[] peerIdBuffer = new byte[PeerHandshakeConstants.PEER_ID_LENGTH];
        buffer.get(peerIdBuffer);
        String peerId = new String(peerIdBuffer);

        if (!peerSession.getPeer().getId().equals(peerId)) {
            if (!peerSession.getPeer().getId().equalsIgnoreCase("12345678901234567890")) {
                System.err.println("Invalid peer id during peer handshake: " + peerId);
                return false;
            }
        }

        return true;
    }
}
