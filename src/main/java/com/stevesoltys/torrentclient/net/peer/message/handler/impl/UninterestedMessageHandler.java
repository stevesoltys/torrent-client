package com.stevesoltys.torrentclient.net.peer.message.handler.impl;

import com.stevesoltys.torrentclient.net.peer.PeerSession;
import com.stevesoltys.torrentclient.net.peer.message.handler.MessageHandler;
import com.stevesoltys.torrentclient.net.peer.message.impl.UninterestedMessage;

/**
 * @author Steve Soltys
 */
public class UninterestedMessageHandler extends MessageHandler<UninterestedMessage> {
    @Override
    public void handle(PeerSession session, UninterestedMessage message) {
        System.out.println("Got uninterested message from peer " + session.getPeer().getId());
    }
}
