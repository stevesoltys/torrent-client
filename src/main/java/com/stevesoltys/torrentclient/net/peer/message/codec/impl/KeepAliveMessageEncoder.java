package com.stevesoltys.torrentclient.net.peer.message.codec.impl;

import com.stevesoltys.torrentclient.net.peer.message.codec.MessageEncoder;
import com.stevesoltys.torrentclient.net.peer.message.impl.KeepAliveMessage;

import java.nio.ByteBuffer;

/**
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class KeepAliveMessageEncoder implements MessageEncoder<KeepAliveMessage> {

    @Override
    public ByteBuffer encode(KeepAliveMessage message) {
        return ByteBuffer.allocate(0);
    }
}
