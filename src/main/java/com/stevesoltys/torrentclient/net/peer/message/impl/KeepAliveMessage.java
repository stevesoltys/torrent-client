package com.stevesoltys.torrentclient.net.peer.message.impl;

import com.stevesoltys.torrentclient.model.Peer;
import com.stevesoltys.torrentclient.net.peer.message.Message;

/**
 * A message sent to keep the connection alive between two {@link Peer}s.
 *
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class KeepAliveMessage extends Message {
}
