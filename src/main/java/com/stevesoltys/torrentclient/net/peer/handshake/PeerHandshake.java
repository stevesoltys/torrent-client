package com.stevesoltys.torrentclient.net.peer.handshake;

import com.stevesoltys.torrentclient.net.peer.PeerSession;
import com.stevesoltys.torrentclient.net.peer.handshake.codec.PeerHandshakeDecoder;
import com.stevesoltys.torrentclient.net.peer.handshake.codec.PeerHandshakeEncoder;
import com.stevesoltys.torrentclient.net.tracker.TorrentSession;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;

/**
 * The handshake performed between two peers before data transfer can begin.
 *
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class PeerHandshake {

    /**
     * The torrent session.
     */
    private final TorrentSession torrentSession;

    /**
     * The peer session.
     */
    private final PeerSession peerSession;

    public PeerHandshake(TorrentSession torrentSession, PeerSession peerSession) {
        this.torrentSession = torrentSession;
        this.peerSession = peerSession;
    }

    /**
     * Performs the handshake.
     *
     * @return A flag indicating whether the handshake was successful.
     */
    public boolean perform() {
        try {
            Socket socket = peerSession.getSocket();

            PeerHandshakeEncoder encoder = new PeerHandshakeEncoder(torrentSession);
            ByteBuffer encodedHandshake = encoder.encode();

            OutputStream outputStream = socket.getOutputStream();

            outputStream.write(encodedHandshake.array());
            outputStream.flush();

            InputStream inputStream = socket.getInputStream();

            byte[] response = new byte[PeerHandshakeConstants.HANDSHAKE_LENGTH];
            int amount = inputStream.read(response);

            if (amount != PeerHandshakeConstants.HANDSHAKE_LENGTH) {
                System.out.println("Invalid handshake length. Expected " +
                        PeerHandshakeConstants.HANDSHAKE_LENGTH + ", got " + amount);
                return false;
            }

            PeerHandshakeDecoder decoder = new PeerHandshakeDecoder(torrentSession, peerSession);
            boolean success = decoder.decode(ByteBuffer.wrap(response));

            return success;

        } catch (IOException e) {
//            e.printStackTrace();
            return false;
        }
    }
}
