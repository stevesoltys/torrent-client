package com.stevesoltys.torrentclient.net.peer;

import com.stevesoltys.torrentclient.model.Peer;
import com.stevesoltys.torrentclient.net.peer.handshake.PeerHandshake;
import com.stevesoltys.torrentclient.net.tracker.TorrentSession;

import java.io.IOException;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Optional;

/**
 * A {@link PeerSession} factory.
 *
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class PeerSessionFactory {

    /**
     * The amount of milliseconds before timing a socket connection out.
     */
    private static final int PEER_TIMEOUT = 100;

    /**
     * The torrent session.
     */
    private TorrentSession torrentSession;

    public PeerSessionFactory(TorrentSession torrentSession) {
        this.torrentSession = torrentSession;
    }

    /**
     * Creates a session for the given peer.
     *
     * @param peer The peer.
     * @return An optional value, possibly containing a peer session.
     */
    public Optional<PeerSession> createSession(Peer peer) {

        try {
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress(peer.getAddress(), peer.getPort()), PEER_TIMEOUT);

            PeerSession peerSession = new PeerSession(torrentSession, peer, socket);

            PeerHandshake handshake = new PeerHandshake(torrentSession, peerSession);
            boolean success = handshake.perform();

            if (success) {
                System.out.println("Successfully established session with peer '" + peer.getId() + "'.");
                return Optional.of(peerSession);
            } else {
                System.out.println("Error establishing session with peer '" + peer.getId() + "', closing.");
                socket.close();
            }

        } catch (ConnectException | SocketTimeoutException e) {
        } catch (IOException e) {
            e.printStackTrace();
            // TODO: Handle exception.
        }

        return Optional.empty();
    }
}
