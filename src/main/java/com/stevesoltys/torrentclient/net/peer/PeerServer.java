package com.stevesoltys.torrentclient.net.peer;

import com.stevesoltys.torrentclient.model.Peer;
import com.stevesoltys.torrentclient.net.peer.handshake.PeerHandshake;
import com.stevesoltys.torrentclient.net.tracker.TorrentSession;
import com.stevesoltys.torrentclient.net.tracker.TorrentSessionState;
import com.stevesoltys.torrentclient.util.ThreadPool;

import java.io.IOException;
import java.net.*;
import java.util.Optional;

/**
 * Listens for incoming connections and binds them to {@link PeerSession}s.
 *
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class PeerServer {

    private static final int PEER_TIMEOUT = 500;

    /**
     * The torrent session.
     */
    private final TorrentSession torrentSession;

    public PeerServer(TorrentSession torrentSession) {
        this.torrentSession = torrentSession;
    }

    /**
     * Starts listening and sending messages between our machine and the {@link Peer} on another thread.
     */
    @SuppressWarnings("unchecked")
    public void start() {
        ThreadPool.getSingleton().submit(() -> {
            try {
                ServerSocket socket = new ServerSocket();

                socket.bind(new InetSocketAddress("0.0.0.0", 6882));
                socket.setSoTimeout(PEER_TIMEOUT);

                while (torrentSession.getState() != TorrentSessionState.STOPPED) {
                    try {
                        Socket sck = socket.accept();
                        if (sck == null) {
                            continue;
                        }

                        InetAddress addr = sck.getInetAddress();

                        System.out.println("Accepted connection from " + addr.getHostAddress());

                        Optional<Peer> optional = findPeerFromAddress(addr);

                        if (optional.isPresent()) {
                            Peer peer = optional.get();

                            PeerSession session = new PeerSession(torrentSession, peer, sck);

                            PeerHandshake handshake = new PeerHandshake(torrentSession, session);
                            boolean success = handshake.perform();

                            if (success) {
                                System.out.println("Successfully registered session with peer: " + peer.getId());
                                torrentSession.registerPeer(session);
                            } else {
                                System.err.println("Error accepting connection from " + peer.getId());
                            }
                        } else {
                            System.err.println("Could not find peer with address: " + addr.getHostAddress());
                        }


                    } catch (SocketTimeoutException e) {
                        // Ignore
                    }
                }

                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }


    private Optional<Peer> findPeerFromAddress(InetAddress address) {
        for (Peer peer : torrentSession.getTorrent().getTracker().getData().getPeers()) {
            if (peer.getAddress().equals(address.getHostAddress())) {
                return Optional.of(peer);
            }
        }

        return Optional.empty();
    }

}
