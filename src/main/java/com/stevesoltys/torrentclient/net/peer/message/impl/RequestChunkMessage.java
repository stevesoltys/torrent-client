package com.stevesoltys.torrentclient.net.peer.message.impl;

import com.stevesoltys.torrentclient.io.TorrentChunk;
import com.stevesoltys.torrentclient.net.peer.message.Message;

/**
 * A message sent to request a block of data from a peer.
 *
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class RequestChunkMessage extends Message {

    /**
     * The max block length for a request.
     */
    public static final int MAX_REQUEST_LENGTH = (int) Math.pow(2, 14);

    /**
     * The {@link TorrentChunk} index.
     */
    private final int index;

    /**
     * The start index, in bytes.
     */
    private final int begin;

    /**
     * The block length, in bytes.
     */
    private final int length;

    public RequestChunkMessage(int index, int begin, int length) {
        this.index = index;
        this.begin = begin;
        this.length = length;
    }

    public int getIndex() {
        return index;
    }

    public int getBegin() {
        return begin;
    }

    public int getLength() {
        return length;
    }
}
