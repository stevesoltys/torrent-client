package com.stevesoltys.torrentclient.net.peer.message.codec.impl;

import com.stevesoltys.torrentclient.net.peer.message.codec.MessageDecoder;
import com.stevesoltys.torrentclient.net.peer.message.impl.HasChunkMessage;

import java.nio.ByteBuffer;

/**
 * @author Steve Soltys
 */
public class HasChunkMessageDecoder implements MessageDecoder<HasChunkMessage> {
    @Override
    public HasChunkMessage decode(ByteBuffer packet) {
        int index = packet.getInt();

        return new HasChunkMessage(index);
    }
}
