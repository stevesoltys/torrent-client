package com.stevesoltys.torrentclient.net.peer.message.handler.impl;

import com.stevesoltys.torrentclient.net.peer.PeerSession;
import com.stevesoltys.torrentclient.net.peer.message.handler.MessageHandler;
import com.stevesoltys.torrentclient.net.peer.message.impl.ChokeMessage;

/**
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class ChokeMessageHandler extends MessageHandler<ChokeMessage> {

    @Override
    public void handle(PeerSession session, ChokeMessage message) {
        session.setChoked(true);
    }
}
