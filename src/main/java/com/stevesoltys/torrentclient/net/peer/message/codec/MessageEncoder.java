package com.stevesoltys.torrentclient.net.peer.message.codec;

import com.stevesoltys.torrentclient.net.peer.message.Message;

import java.nio.ByteBuffer;

/**
 * An encoder for a {@link Message}.
 *
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public interface MessageEncoder<T extends Message> {

    ByteBuffer encode(T message);
}
