package com.stevesoltys.torrentclient.net.peer.message.codec.impl;

import com.stevesoltys.torrentclient.net.peer.message.codec.MessageDecoder;
import com.stevesoltys.torrentclient.net.peer.message.impl.InterestedMessage;

import java.nio.ByteBuffer;

/**
 * @author Steve Soltys
 */
public class InterestedMessageDecoder implements MessageDecoder<InterestedMessage> {
    @Override
    public InterestedMessage decode(ByteBuffer packet) {
        return new InterestedMessage();
    }
}
