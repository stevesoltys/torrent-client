package com.stevesoltys.torrentclient.net.peer.message.impl;

import com.stevesoltys.torrentclient.io.TorrentChunk;
import com.stevesoltys.torrentclient.net.peer.message.Message;

import java.util.Arrays;

/**
 * A message which contains part of a {@link TorrentChunk}.
 *
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class ChunkMessage extends Message {

    /**
     * The {@link TorrentChunk} index.
     */
    private final int index;

    /**
     * The start index.
     */
    private final int begin;

    /**
     * The data.
     */
    private final byte[] data;

    public ChunkMessage(int index, int begin, byte[] data) {
        this.index = index;
        this.begin = begin;
        this.data = data;
    }

    @Override
    public String toString() {
        return "ChunkMessage{" +
                "index=" + index +
                ", begin=" + begin +
                ", data=" + Arrays.toString(data) +
                '}';
    }

    public int getIndex() {
        return index;
    }

    public int getBegin() {
        return begin;
    }

    public byte[] getData() {
        return data;
    }
}
