package com.stevesoltys.torrentclient.net.peer.message.handler.impl;

import com.stevesoltys.torrentclient.io.TorrentChunk;
import com.stevesoltys.torrentclient.net.peer.PeerSession;
import com.stevesoltys.torrentclient.net.peer.message.handler.MessageHandler;
import com.stevesoltys.torrentclient.net.peer.message.impl.ChunkMessage;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.Optional;

/**
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class ChunkMessageHandler extends MessageHandler<ChunkMessage> {
    @Override
    public void handle(PeerSession session, ChunkMessage message) {

        List<TorrentChunk> chunks = session.getTorrentSesssion().getTorrent().getChunks();

        if (message.getIndex() < chunks.size()) {
            TorrentChunk chunk = chunks.get(message.getIndex());

            ByteBuffer buffer = chunk.getData();
            buffer.position(message.getBegin());
            buffer.put(message.getData());

            session.setCurrentChunk(Optional.of(chunk));
        }

    }
}
