package com.stevesoltys.torrentclient.net.peer.message.handler.impl;

import com.stevesoltys.torrentclient.net.peer.PeerSession;
import com.stevesoltys.torrentclient.net.peer.message.handler.MessageHandler;
import com.stevesoltys.torrentclient.net.peer.message.impl.InterestedMessage;
import com.stevesoltys.torrentclient.net.peer.message.impl.UnchokeMessage;

/**
 * @author Steve Soltys
 */
public class InterestedMessageHandler extends MessageHandler<InterestedMessage> {
    @Override
    public void handle(PeerSession session, InterestedMessage message) {
        System.out.println("Got interested message from peer " + session.getPeer().getId());

        session.setPeerChoked(false);
        session.send(new UnchokeMessage());
    }
}
