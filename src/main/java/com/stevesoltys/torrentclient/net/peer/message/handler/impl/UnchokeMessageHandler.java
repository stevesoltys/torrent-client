package com.stevesoltys.torrentclient.net.peer.message.handler.impl;

import com.stevesoltys.torrentclient.net.peer.PeerSession;
import com.stevesoltys.torrentclient.net.peer.message.handler.MessageHandler;
import com.stevesoltys.torrentclient.net.peer.message.impl.UnchokeMessage;

/**
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class UnchokeMessageHandler extends MessageHandler<UnchokeMessage> {

    @Override
    public void handle(PeerSession session, UnchokeMessage message) {
        session.setChoked(false);
    }
}
