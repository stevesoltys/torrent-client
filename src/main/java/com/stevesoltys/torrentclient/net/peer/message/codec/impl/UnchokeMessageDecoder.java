package com.stevesoltys.torrentclient.net.peer.message.codec.impl;

import com.stevesoltys.torrentclient.net.peer.message.codec.MessageDecoder;
import com.stevesoltys.torrentclient.net.peer.message.impl.UnchokeMessage;

import java.nio.ByteBuffer;

/**
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class UnchokeMessageDecoder implements MessageDecoder<UnchokeMessage> {

    @Override
    public UnchokeMessage decode(ByteBuffer packet) {
        return new UnchokeMessage();
    }
}
