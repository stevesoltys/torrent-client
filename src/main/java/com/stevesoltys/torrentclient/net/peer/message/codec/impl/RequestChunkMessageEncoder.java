package com.stevesoltys.torrentclient.net.peer.message.codec.impl;

import com.stevesoltys.torrentclient.net.peer.message.codec.MessageEncoder;
import com.stevesoltys.torrentclient.net.peer.message.impl.RequestChunkMessage;

import java.nio.ByteBuffer;

/**
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class RequestChunkMessageEncoder implements MessageEncoder<RequestChunkMessage> {
    @Override
    public ByteBuffer encode(RequestChunkMessage message) {

        ByteBuffer buffer = ByteBuffer.allocate(13);

        buffer.put((byte) 6);
        buffer.putInt(message.getIndex());
        buffer.putInt(message.getBegin());
        buffer.putInt(message.getLength());

        return buffer;
    }
}
