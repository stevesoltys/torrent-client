package com.stevesoltys.torrentclient.net.tracker;

import com.stevesoltys.torrentclient.model.Tracker;
import com.stevesoltys.torrentclient.model.TrackerData;
import com.stevesoltys.torrentclient.util.Bencoder2;
import com.stevesoltys.torrentclient.util.BencodingException;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

/**
 * A parser for obtaining the {@link TrackerData} from a {@link Tracker}'s response.
 *
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class TrackerResponseParser {

    /**
     * Parses the {@link TrackerData} from a {@link Tracker}'s response.
     *
     * @param responseStream The response stream.
     * @return The tracker data.
     * @throws IOException If there is an error while reading the data.
     * @throws BencodingException If there is an error while decoding the data.
     */
    @SuppressWarnings("unchecked")
    public TrackerData parse(InputStream responseStream) throws IOException, BencodingException {
        byte[] data = new byte[responseStream.available()];
        int bytes = responseStream.read(data);
        responseStream.close();

        Map<ByteBuffer, Object> byteBufferDictionary = (Map<ByteBuffer, Object>) Bencoder2.decode(data);

        Map<String, Object> dictionary = new HashMap<>();

        byteBufferDictionary.keySet().stream().forEach(key ->
                dictionary.put(new String(key.array()), byteBufferDictionary.get(key)));

        return new TrackerData(dictionary);
    }
}
