package com.stevesoltys.torrentclient.net.tracker;

import com.stevesoltys.torrentclient.io.TorrentFileManager;
import com.stevesoltys.torrentclient.model.Peer;
import com.stevesoltys.torrentclient.model.Torrent;
import com.stevesoltys.torrentclient.model.Tracker;
import com.stevesoltys.torrentclient.model.TrackerData;
import com.stevesoltys.torrentclient.net.peer.PeerServer;
import com.stevesoltys.torrentclient.net.peer.PeerSession;
import com.stevesoltys.torrentclient.net.peer.PeerSessionFactory;
import com.stevesoltys.torrentclient.util.BencodingException;
import com.stevesoltys.torrentclient.util.ThreadPool;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * A session between the local machine and a torrent's tracker.
 *
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class TorrentSession {

    /**
     * THe maximum amount of peers that a torrent session can have.
     */
    private static final int MAXIMUM_PEERS = 50;

    /**
     * The hardcoded peer addresses for determining whether we want to download from a peer.
     */
    private static final List<String> PEER_ADDRESSES = new LinkedList<>();

    static {
        PEER_ADDRESSES.add("128.6.171.130");
        PEER_ADDRESSES.add("128.6.171.131");
    }

    /**
     * The torrent.
     */
    private final Torrent torrent;

    /**
     * The torrent file manager.
     */
    private final TorrentFileManager torrentFileManager;

    /**
     * The pending peer queue.
     */
    private final Queue<PeerSession> pendingPeers = new ConcurrentLinkedQueue<>();

    /**
     * A map containing our active sessions.
     */
    private final Map<Peer, PeerSession> sessions;

    /**
     * Our peer id, generated on construction of the session.
     */
    private final String peerId;

    /**
     * The current session state.
     */
    private TorrentSessionState state;

    /**
     * The scheduled time for the tracker to be updated next.
     */
    private long nextTrackerUpdate;

    public TorrentSession(Torrent torrent, TorrentFileManager torrentFileManager) {
        this.torrent = torrent;
        this.torrentFileManager = torrentFileManager;
        this.sessions = new HashMap<>();
        this.peerId = generatePeerId();
        this.state = TorrentSessionState.IDLE;
        this.nextTrackerUpdate = System.currentTimeMillis();
    }

    /**
     * Starts this torrent session.
     *
     * @throws IOException
     * @throws InterruptedException
     * @throws BencodingException
     */
    public void start() throws BencodingException {
        state = TorrentSessionState.STARTED;

        ThreadPool.getSingleton().submit(() -> {

            updateTracker(); // Update the tracker before we start listening for connections.

            PeerServer peerServer = new PeerServer(this);
            peerServer.start();

            while (state != TorrentSessionState.STOPPED) {
                handlePendingPeers();

                if (torrentFileManager.completed()) {
                    if (state == TorrentSessionState.COMPLETED) {
                        updateTracker();
                    }

                    state = TorrentSessionState.IDLE;
                } else {
                    state = TorrentSessionState.DOWNLOADING;
                    torrentFileManager.getTorrentProgress().updateDownloadTime();
                }

                if (System.currentTimeMillis() >= nextTrackerUpdate) {
                    updateTracker();
                }

                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            updateTracker();

            sessions.values().forEach(PeerSession::stop);
        });
    }

    public void registerPeer(PeerSession session) {
        pendingPeers.offer(session);
    }

    private void handlePendingPeers() {
        while (!pendingPeers.isEmpty()) {
            PeerSession peer = pendingPeers.poll();

            if (!sessions.containsKey(peer.getPeer())) {
                peer.start();

                sessions.put(peer.getPeer(), peer);
            } else {
                System.err.println("Found duplicate peer '" + peer.getPeer().getId() + "', closing connection.");
                peer.stop();
            }
        }
    }

    /**
     * Updates the tracker with our current state and parses the response.
     *
     * @throws IOException        If an error occurs while sending the tracker our state.
     * @throws BencodingException If an error occurs while decoding the response.
     */
    public void updateTracker() {
        try {
            Tracker tracker = torrent.getTracker();

            String uri = tracker.getAnnounceUrl() +
                    "?info_hash=" + torrent.getEscapedInfoHash() +
                    "&peer_id=" + peerId +
                    "&port=" + 6882 +
                    "&uploaded=" + String.valueOf(torrentFileManager.getBytesUploaded()) +
                    "&downloaded=" + String.valueOf(torrentFileManager.getBytesDownloaded()) +
                    "&left=" + String.valueOf(torrentFileManager.getRemainingBytes()) +
                    "&event=" + state.getName();

            TrackerResponseParser responseParser = new TrackerResponseParser();
            TrackerData data = responseParser.parse((InputStream) new URL(uri).getContent());

            torrent.getTracker().setData(data);

            nextTrackerUpdate = System.currentTimeMillis() + (data.getInterval() * 1000);

            updatePeers();

        } catch (IOException | BencodingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets peers from the peer list and attempts to create a session with them.
     */
    private void updatePeers() {
        TrackerData trackerData = torrent.getTracker().getData();

        PeerSessionFactory sessionFactory = new PeerSessionFactory(this);
        trackerData.getPeers().stream().filter(peer -> !sessions.containsKey(peer)).forEach(peer -> {

            if (sessions.size() < MAXIMUM_PEERS) {
                Optional<PeerSession> session = sessionFactory.createSession(peer);

                if (session.isPresent()) {
                    session.get().start();
                    sessions.put(peer, session.get());
                }
            }

        });

    }

    public void unregisterPeer(Peer peer) {
        if (sessions.containsKey(peer)) {
            System.out.println("Unregistered peer: " + peer.getId());
            sessions.remove(peer);
        }
    }

    /**
     * Generates a peer id for this session.
     *
     * @return The generated peer id.
     */
    private String generatePeerId() {
        StringBuilder builder = new StringBuilder();

        Random random = new Random();

        while (builder.length() < 20) {
            builder.append((char) (random.nextInt(26) + 'a'));
        }

        return builder.toString();
    }

    public Torrent getTorrent() {
        return torrent;
    }

    public TorrentFileManager getFileManager() {
        return torrentFileManager;
    }

    public String getPeerId() {
        return peerId;
    }

    public TorrentSessionState getState() {
        return state;
    }

    public void setState(TorrentSessionState state) {
        this.state = state;
    }

    public Map<Peer, PeerSession> getSessions() {
        return sessions;
    }
}
