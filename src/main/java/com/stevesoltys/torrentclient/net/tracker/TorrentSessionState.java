package com.stevesoltys.torrentclient.net.tracker;

/**
 * An enumeration of states that a {@link TorrentSession} can be in.
 *
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public enum TorrentSessionState {
    IDLE("empty"),
    DOWNLOADING("empty"),
    STARTED("started"),
    STOPPED("stopped"),
    COMPLETED("completed");

    private final String name;

    TorrentSessionState(String event) {
        this.name = event;
    }

    public String getName() {
        return name;
    }
}
