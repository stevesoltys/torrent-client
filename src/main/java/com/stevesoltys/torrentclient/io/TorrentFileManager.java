package com.stevesoltys.torrentclient.io;

import com.stevesoltys.torrentclient.model.Torrent;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

/**
 * A file manager for a {@link Torrent}.
 *
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class TorrentFileManager {

    /**
     * The torrent.
     */
    private final Torrent torrent;

    /**
     * The torrent progress.
     */
    private final TorrentProgress torrentProgress;

    /**
     * The file.
     */
    private final File outputFile;

    public TorrentFileManager(Torrent torrent, File outputFile) {
        this.torrent = torrent;
        this.outputFile = outputFile;

        File progressFile = new File(outputFile.getAbsolutePath() + ".ini");

        if (progressFile.exists()) {
            torrentProgress = new TorrentProgress(progressFile);
        } else {
            torrentProgress = new TorrentProgress();
        }

        if (outputFile.exists()) {
            try (FileInputStream fis = new FileInputStream(outputFile)) {
                byte[] data = new byte[torrent.getFileLength()];
                int amount = fis.read(data);

                if (amount != torrent.getFileLength()) {
                    throw new IOException("Mismatched file length.");
                }

                for (TorrentChunk chunk : torrent.getChunks()) {
                    int fileIndex = chunk.getIndex() * torrent.getChunkSize();

                    byte[] chunkData = Arrays.copyOfRange(data, fileIndex, fileIndex + chunk.getLength());

                    chunk.getData().put(chunkData);
                    chunk.setVerified(chunk.verify());

                    if (!chunk.isVerified()) {
                        chunk.getData().position(0);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Gets a bitfield for this torrent.
     *
     * @return The bitfield array.
     */
    public boolean[] getBitfield() {

        List<TorrentChunk> chunks = torrent.getChunks();

        boolean[] bitfield = new boolean[chunks.size()];

        for (int i = 0; i < chunks.size(); i++) {
            bitfield[i] = chunks.get(i).isVerified();
        }

        return bitfield;
    }

    /**
     * Gets a random chunk from our {@link Torrent}'s chunk list that hasn't been verified yet.
     *
     * @return The random chunk.
     */
    public Optional<TorrentChunk> getRandomIncompleteChunk() {

        List<TorrentChunk> chunkList = new LinkedList<>();
        chunkList.addAll(torrent.getChunks());
        Collections.shuffle(chunkList);

        for (TorrentChunk chunk : chunkList) {
            if (!chunk.isVerified()) {
                return Optional.of(chunk);
            }
        }

        return Optional.empty();
    }

    /**
     * Checks whether or not this torrent has finished downloading.
     *
     * @return Whether or not this torrent completed.
     */
    public boolean completed() {
        return getChunksDownloaded() == torrent.getChunks().size();
    }

    /**
     * Writes the chunks of data that we have obtained so far to the given file.
     *
     * @throws IOException If an error occurs while writing.
     */
    public void writeFile() throws IOException {
        try (FileOutputStream fos = new FileOutputStream(outputFile)) {

            for (TorrentChunk chunk : torrent.getChunks()) {
                fos.write(chunk.getData().array());
            }
        }
    }

    /**
     * Gets the number of chunks that we have verified so far.
     *
     * @return The number of chunks verified so far.
     */
    public long getChunksDownloaded() {
        int chunks = 0;

        for (TorrentChunk chunk : torrent.getChunks()) {
            if (chunk.isVerified()) {
                chunks++;
            }
        }

        return chunks;
    }

    /**
     * Gets the number of bytes that remain to have been downloaded for this torrent.
     *
     * @return The number of bytes remaining.
     */
    public long getRemainingBytes() {
        return torrent.getFileLength() - getBytesDownloaded();
    }

    /**
     * Gets the number of bytes that have been downloaded for this torrent.
     *
     * @return The number of bytes downloaded.
     */
    public long getBytesDownloaded() {
        return getChunksDownloaded() * torrent.getChunkSize();
    }

    /**
     * Gets the number of bytes that have been uploaded for this torrent.
     *
     * @return The number of bytes downloaded.
     */
    public long getBytesUploaded() {
        return torrentProgress.getUploaded() * torrent.getChunkSize();
    }

    /**
     * Gets the current progress of this torrent.
     *
     * @return The torrent's progress.
     */
    public TorrentProgress getTorrentProgress() {
        return torrentProgress;
    }
}
