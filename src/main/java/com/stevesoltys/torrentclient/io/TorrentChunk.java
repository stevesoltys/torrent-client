package com.stevesoltys.torrentclient.io;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 * An indexed chunk of data for this torrent.
 *
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class TorrentChunk {

    /**
     * The SHA-1 hash data for this chunk.
     */
    private final ByteBuffer hash;

    /**
     * The index for this chunk.
     */
    private final int index;

    /**
     * The length in bytes for this chunk.
     */
    private final int length;

    /**
     * A buffer containing the data we currently have for this chunk.
     */
    private final ByteBuffer data;

    /**
     * A flag indicating whether or not this chunk has been verified.
     */
    private boolean verified;

    public TorrentChunk(ByteBuffer hash, int index, int length) {
        this.hash = hash;
        this.index = index;
        this.length = length;
        this.data = ByteBuffer.allocate(length);
        this.verified = false;
    }

    /**
     * Checks the data in this chunk against the SHA-1 hash.
     *
     * @return A flag indicating whether or not this chunk is valid.
     */
    public boolean verify() {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            digest.update(data.array());
            byte[] dataHash = digest.digest();

            return Arrays.equals(dataHash, hash.array());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return false;
        }
    }

    public ByteBuffer getHash() {
        return hash;
    }

    public int getIndex() {
        return index;
    }

    public ByteBuffer getData() {
        return data;
    }

    public int getLength() {
        return length;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }
}
