package com.stevesoltys.torrentclient.io;

import java.io.*;
import java.util.Properties;

/**
 * @author Steve Soltys
 */
public class TorrentProgress {

    private int uploaded;

    private long startDownloadTime;

    private long downloadTime;

    public TorrentProgress(File file) {
        this.uploaded = 0;
        this.downloadTime = 0;
        this.startDownloadTime = System.nanoTime();

        if (file != null) {
            load(file);
        }
    }

    public TorrentProgress() {
        this(null);
    }

    private void load(File file) {

        try (FileInputStream fis = new FileInputStream(file)) {
            Properties properties = new Properties();
            properties.load(fis);

            this.uploaded = Integer.parseInt(properties.getProperty("uploaded"));
            this.downloadTime = Long.parseLong(properties.getProperty("time"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void save(File file) {
        try (FileOutputStream fos = new FileOutputStream(file)) {
            Properties properties = new Properties();
            properties.put("uploaded", String.valueOf(uploaded));
            properties.put("time", String.valueOf(downloadTime));
            properties.store(fos, "torrent progress information");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public int getUploaded() {
        return uploaded;
    }

    public void increaseUploaded(long amount) {
        uploaded += amount;
    }

    public long getTimeElapsed() {
        return downloadTime;
    }

    public void updateDownloadTime() {
        this.downloadTime = downloadTime + (System.nanoTime() - startDownloadTime);
        this.startDownloadTime = System.nanoTime();
    }

}
