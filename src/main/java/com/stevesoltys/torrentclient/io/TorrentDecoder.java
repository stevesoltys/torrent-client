package com.stevesoltys.torrentclient.io;

import com.stevesoltys.torrentclient.model.Torrent;
import com.stevesoltys.torrentclient.util.BencodingException;
import com.stevesoltys.torrentclient.util.TorrentInfo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * A decoder for the given torrent {@link File}.
 *
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class TorrentDecoder {

    /**
     * The torrent file.
     */
    private File file;

    public TorrentDecoder(File file) {
        this.file = file;
    }

    /**
     * Parses the torrent file.
     *
     * @return The resulting torrent instance.
     * @throws IOException If there was an error reading the file.
     * @throws BencodingException If there was an error decoding the file.
     */
    public Torrent decode() throws IOException, BencodingException {

        FileInputStream fis = new FileInputStream(file);
        byte[] data = new byte[(int) file.length()];
        int bytes = fis.read(data);
        fis.close();

        return new Torrent(new TorrentInfo(data));
    }
}
