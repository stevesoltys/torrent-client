package com.stevesoltys.torrentclient;

import com.stevesoltys.torrentclient.io.TorrentDecoder;
import com.stevesoltys.torrentclient.io.TorrentFileManager;
import com.stevesoltys.torrentclient.io.TorrentProgress;
import com.stevesoltys.torrentclient.model.Torrent;
import com.stevesoltys.torrentclient.net.tracker.TorrentSession;
import com.stevesoltys.torrentclient.net.tracker.TorrentSessionState;
import com.stevesoltys.torrentclient.util.BencodingException;
import com.stevesoltys.torrentclient.util.ThreadPool;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * The entry point to this BitTorrent client.
 *
 * @author Steve Soltys
 * @author Kaitlyn Sussman
 * @author Phillip Beauchea
 */
public class RUBTClient {

    public static void main(String[] args) throws InterruptedException {
        try {

            if (args.length < 2) {
                System.err.println("Invalid usage. Correct usage: 'java RUBTClient [torrent] [output]'.");
                System.exit(1);
            }

            TorrentDecoder decoder = new TorrentDecoder(new File(args[0]));
            Torrent torrent = decoder.decode();

            File outputFile = new File(args[1]);
            TorrentFileManager fileManager = new TorrentFileManager(torrent, outputFile);

            TorrentSession torrentSession = new TorrentSession(torrent, fileManager);
            torrentSession.start();

            Scanner scanner = new Scanner(System.in);

            while (torrentSession.getState() != TorrentSessionState.STOPPED) {
                if (scanner.hasNextLine()) {
                    String input = scanner.nextLine();

                    if (input.equalsIgnoreCase("q")) {
                        torrentSession.setState(TorrentSessionState.STOPPED);
                        fileManager.getTorrentProgress().save(new File(outputFile.getAbsolutePath() + ".ini"));

                        break;
                    } else if (input.equalsIgnoreCase("s")) {
                        System.out.println("Current status: ");

                        TorrentProgress torrentProgress = fileManager.getTorrentProgress();

                        System.out.println("Time elapsed (seconds): " + TimeUnit.SECONDS.convert(
                                torrentProgress.getTimeElapsed(), TimeUnit.NANOSECONDS));

                        System.out.println("Chunks downloaded: " + fileManager.getChunksDownloaded() + " out of " +
                                torrent.getChunks().size());

                        System.out.println("Bytes uploaded: " + torrentProgress.getUploaded());

                        System.out.println("Torrent session state: " + torrentSession.getState().toString());
                    }

                    System.out.print("-> ");
                    System.out.flush();
                }

                Thread.sleep(1);
            }

            fileManager.writeFile();

            ThreadPool.getSingleton().shutdown();

        } catch (IOException | BencodingException e) {
            e.printStackTrace();
        }

    }
}
